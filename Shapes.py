
class Shapes (object):
    def __init__(self, name, image):
          self.name = name
          self.image = image

format = [Shapes("Mimela", "circle.png"), Shapes("Oblothun", "square.png"), Shapes("Oiseyamni", "triangle.png"), Shapes("Mimela Hanska", "oval.png"), Shapes("Oblothun Hanska", "rectangle.png"), Shapes("Otkunza", "parallelogram.png"), Shapes("Anung Phestola", "diamond.png"), Shapes("Oblothun Osteka", "trapezoid.png")]
image = dict([ (f.name, f.image) for f in format ])
