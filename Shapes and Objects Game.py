#### Created by Rissa Snider
#### Property of the Lakota Immersion Childcare

import os, json, re
import pygame
import random
import flask

from flask import (
              Flask,
              session,
              render_template,
              redirect,
              url_for,
              request,
              make_response,
)

app = Flask(__name__)

app.config['SECRET_KEY'] = 'Noes923REEO';

''' Game Logic Code is Below (controller) ''' 

@app.route('/')
def index( ):
             session.clear()
             return render_template("Lakota Language Game.html", score = 0)

@app.route('/game', methods=['GET', 'POST'])
def game( ):
                session['word'] = request.form['word
